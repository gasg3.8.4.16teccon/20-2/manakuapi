-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-12-2020 a las 06:45:07
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `makuapi`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datascrapings`
--

CREATE TABLE `datascrapings` (
  `id` int(11) NOT NULL,
  `description` varchar(250) NOT NULL,
  `state` varchar(200) NOT NULL,
  `company` varchar(200) NOT NULL,
  `url` varchar(200) DEFAULT NULL,
  `categoria` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial_webscrapings`
--

CREATE TABLE `historial_webscrapings` (
  `id` int(11) NOT NULL,
  `fecha` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `historial_webscrapings`
--

INSERT INTO `historial_webscrapings` (`id`, `fecha`, `created_at`, `updated_at`) VALUES
(19, '2020-12-09 02:40:23', '2020-12-09 07:40:23', '2020-12-09 07:40:23'),
(20, '2020-12-09 03:17:11', '2020-12-09 08:17:11', '2020-12-09 08:17:11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1),
(9, '2019_10_11_123643_create_products_table', 1),
(10, '2020_09_30_202902_create_page_web', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('0447f6437af014f851d971fd19483a1b3811624deef23e45af4a0647d79167da6d09005fc8051815', 3, 1, 'MyApp', '[]', 0, '2020-11-12 02:19:13', '2020-11-12 02:19:13', '2021-11-11 21:19:13'),
('0aa33ab923a3129954398ce8c507e2bd46c500f062a6aff222c235247e9f7c7ff07248909ebd548a', 18, 1, 'MyApp', '[]', 0, '2020-10-02 22:46:03', '2020-10-02 22:46:03', '2021-10-02 17:46:03'),
('0d37ebbfdb70dd12026640e881aa56567310d6244ed4aff53f8eb46290303d2f03e0dc6f716e1db0', 22, 1, 'MyApp', '[]', 0, '2020-10-23 07:14:22', '2020-10-23 07:14:22', '2021-10-23 02:14:22'),
('0dd43271bdef24376ad3fefa11dfc61894f08c17f15ce684657e462da2fe89c24bfe64b61bbf4b5e', 14, 1, 'MyApp', '[]', 0, '2020-10-02 22:42:53', '2020-10-02 22:42:53', '2021-10-02 17:42:53'),
('1559ce84b5542047c42f1c66f5a9a2426200d3d5067edadee4adecdaf52871d298b9a0c148191a3a', 12, 1, 'MyApp', '[]', 0, '2020-10-02 22:41:45', '2020-10-02 22:41:45', '2021-10-02 17:41:45'),
('180aefe8b378a949f66b1602a5d0da2402eba18313cce8b2422e11e387e461fdba06a0354800dff0', 21, 1, 'MyApp', '[]', 0, '2020-10-04 05:36:19', '2020-10-04 05:36:19', '2021-10-04 00:36:19'),
('1849e9011aa717c0d3ccf1ff2b4f7b047a7a06e9232a8b377518160623a0fee46dcf42d35186719d', 3, 1, 'MyApp', '[]', 0, '2020-10-23 07:12:08', '2020-10-23 07:12:08', '2021-10-23 02:12:08'),
('1936c9bb43f385645722d9694e9194dc1d01b50db5de94f70c0619ad24fb69129b9c034c0b94b1fa', 3, 1, 'MyApp', '[]', 0, '2020-11-12 02:17:23', '2020-11-12 02:17:23', '2021-11-11 21:17:23'),
('2b47c35e6c62b87e88c009c3294a9e580c695d2710a8567cde493b5a3d748fc36a571ae7d1a5fb61', 14, 1, 'MyApp', '[]', 0, '2020-10-02 22:42:53', '2020-10-02 22:42:53', '2021-10-02 17:42:53'),
('38ecf93aa1d2b0e5484b78f50eb631e23f7fedc16c5e0d1a1c49556bffb02dfb6170cc8aaaa206e5', 19, 1, 'MyApp', '[]', 0, '2020-10-02 22:46:09', '2020-10-02 22:46:09', '2021-10-02 17:46:09'),
('39316d818ab03e12d17663ca2bc5124a068a9976752fddf6976a610439ab669ccb93b06eba185ffc', 15, 1, 'MyApp', '[]', 0, '2020-10-02 22:43:02', '2020-10-02 22:43:02', '2021-10-02 17:43:02'),
('4001da6e218b097bb655c3f24aa5b48e9fc1430ebd0c984ff7ecdff721390e76c2495f30a965aa24', 9, 1, 'MyApp', '[]', 0, '2020-10-02 22:40:41', '2020-10-02 22:40:41', '2021-10-02 17:40:41'),
('4070025726d1b5f80ad6e886d1a043c72042690bda34e80dd4330fbde1d34e4f31d9c4d3a51a9d57', 7, 1, 'MyApp', '[]', 0, '2020-10-02 22:39:26', '2020-10-02 22:39:26', '2021-10-02 17:39:26'),
('419ff1a95b8ea745bdacadd198d8b361ddf1d6c356dac43eda572a3df001f23734d2b9a9f12b8720', 3, 1, 'MyApp', '[]', 0, '2020-10-04 05:35:35', '2020-10-04 05:35:35', '2021-10-04 00:35:35'),
('4410cc539c9869944f05c0192e7721d05781c8df25811dc95f87e6bcf3903d211beca6e7bc296972', 20, 1, 'MyApp', '[]', 0, '2020-10-02 22:46:22', '2020-10-02 22:46:22', '2021-10-02 17:46:22'),
('45f16006a93bc5eeec1abe6dd1d43acd6ae9d656e57ea0112264afa5d6a9d6ccd03c42693b2e5c42', 19, 1, 'MyApp', '[]', 0, '2020-10-02 22:46:09', '2020-10-02 22:46:09', '2021-10-02 17:46:09'),
('4d16c54399637a874988d52fb8e81836f3a9c2483dfe50716b76be7c720acdbcba3176c8969082cf', 6, 1, 'MyApp', '[]', 0, '2020-10-02 22:38:18', '2020-10-02 22:38:18', '2021-10-02 17:38:18'),
('4e47cf5627e9b5512f61927bca70f98e88a1be9e93c24d3d7dd7dd32a680100c3d27d12cb7980f89', 3, 1, 'MyApp', '[]', 0, '2020-10-02 22:45:43', '2020-10-02 22:45:43', '2021-10-02 17:45:43'),
('55e387201347e1e40ab561a0c8f2bf344c52876e4720a0c655ec364e2ac7b31c5bb412b4ebfa40db', 3, 1, 'MyApp', '[]', 0, '2020-10-02 22:37:36', '2020-10-02 22:37:36', '2021-10-02 17:37:36'),
('57fdca6554b1b92f2fccf643eb553fcf4cc59bb7092c8b10b8b8b183dd599b1bc016758a68d8940d', 10, 1, 'MyApp', '[]', 0, '2020-10-02 22:40:46', '2020-10-02 22:40:46', '2021-10-02 17:40:46'),
('5b96ad7521875b697297cb5776cf3442a86f366035c19eb8280f3bab04fcc91fd094f567640c0e69', 23, 1, 'MyApp', '[]', 0, '2020-11-27 11:17:23', '2020-11-27 11:17:23', '2021-11-27 06:17:23'),
('5cafd94f6896040e7799afe7d082d77dd83d3b72206bb77fef7b1104f80855834acd7201ae15dfac', 8, 1, 'MyApp', '[]', 0, '2020-10-02 22:39:35', '2020-10-02 22:39:35', '2021-10-02 17:39:35'),
('5dd9ead8728a5286d7e929a45eebc1d8831e1f0e34a7c79e279b289faa539f43e86917b7ba73a688', 7, 1, 'MyApp', '[]', 0, '2020-10-02 22:39:26', '2020-10-02 22:39:26', '2021-10-02 17:39:26'),
('5f22676c205bbf6c4d7cf039af0ac575f4c71d83159e53bac7144268614b1c3b01a12cdfd26ef860', 18, 1, 'MyApp', '[]', 0, '2020-10-02 22:46:02', '2020-10-02 22:46:02', '2021-10-02 17:46:02'),
('60299a053cf9339a076670eca5e561353534d8b4627bd9663d6a939874657b579819c2220ee46bb8', 8, 1, 'MyApp', '[]', 0, '2020-10-02 22:39:35', '2020-10-02 22:39:35', '2021-10-02 17:39:35'),
('6259d31599305a22ebf91304ee1029d49f31a9c4558aa0e68735c98f70fccfb98171db0b7e4ecf36', 17, 1, 'MyApp', '[]', 0, '2020-10-02 22:45:58', '2020-10-02 22:45:58', '2021-10-02 17:45:58'),
('64dfa1dd95135001f8157a21db121a3bedfade3e79ad03018eb5f1ac3df80a5a6692d81e0a6ecd20', 17, 1, 'MyApp', '[]', 0, '2020-10-02 22:45:58', '2020-10-02 22:45:58', '2021-10-02 17:45:58'),
('6a570ced321ecefe018a872b386cc906958dc29765e1a6dd107c96fc9ddf14f295882efb6b477f95', 3, 1, 'MyApp', '[]', 0, '2020-10-02 22:43:47', '2020-10-02 22:43:47', '2021-10-02 17:43:47'),
('704a1d9a5f967a3680c3916ec07fb9571a08afce25f815bd6ff23d4a2ab4fc450758e5bf2cb6c15b', 3, 1, 'MyApp', '[]', 0, '2020-10-04 05:27:31', '2020-10-04 05:27:31', '2021-10-04 00:27:31'),
('76adf25d1c0bc82f57af5abb9f6ac0de5f9acc6aabc3f75e183e18486f1d55a45aa9f8ca9cf5f220', 16, 1, 'MyApp', '[]', 0, '2020-10-02 22:45:12', '2020-10-02 22:45:12', '2021-10-02 17:45:12'),
('7baafe7f3bac979f98fe4bfeb4858a664964d0e47d107f564f3d8710f1bc2502f23b37851a64b23e', 3, 1, 'MyApp', '[]', 0, '2020-10-23 07:12:57', '2020-10-23 07:12:57', '2021-10-23 02:12:57'),
('7c6f5473432403dca1d20cd2469e0094ba9222f2da5e485faebe45b375224eecf14daaa06c2d4518', 3, 1, 'MyApp', '[]', 0, '2020-10-09 07:46:07', '2020-10-09 07:46:07', '2021-10-09 02:46:07'),
('8d06cdff5c87dd05f80db07c34a2ebc5d5dde9a2eeafe26ec2ebd7b2045403eff7f32d593b96ae89', 20, 1, 'MyApp', '[]', 0, '2020-10-02 22:46:23', '2020-10-02 22:46:23', '2021-10-02 17:46:23'),
('94ee84201699ffc5ead581a0e7ec13d5b630a9ae14af53fdc18de63dbf8c94da3d0e7753af49d5be', 11, 1, 'MyApp', '[]', 0, '2020-10-02 22:40:55', '2020-10-02 22:40:55', '2021-10-02 17:40:55'),
('968fac814b915dd8e6694092d70646517c9632a01cca43024f539809c88d65d3505b44ba365953be', 6, 1, 'MyApp', '[]', 0, '2020-10-02 22:38:18', '2020-10-02 22:38:18', '2021-10-02 17:38:18'),
('a0c84d325e5473c4dca440160d8c479e65bd2a2de034aef5ccafa60beba5b50bc50bbba48c0d672f', 11, 1, 'MyApp', '[]', 0, '2020-10-02 22:40:55', '2020-10-02 22:40:55', '2021-10-02 17:40:55'),
('b5faebce1b90cd30b2d52989cd5cb165076d6f58af47cbfcb935d3201306ce2832ff4ea10dbaed1c', 21, 1, 'MyApp', '[]', 0, '2020-10-04 05:36:19', '2020-10-04 05:36:19', '2021-10-04 00:36:19'),
('b6d437300247041332a867f412682a3bef890e071707063728b7fe1a8a15fe977adfce48a8e2a9bc', 12, 1, 'MyApp', '[]', 0, '2020-10-02 22:41:45', '2020-10-02 22:41:45', '2021-10-02 17:41:45'),
('bbd8b9068cc94f2791e9120c9bc4167141247563b765ca4f0a0489769d3ba2d564d564a38d39f26f', 3, 1, 'MyApp', '[]', 0, '2020-10-23 07:16:37', '2020-10-23 07:16:37', '2021-10-23 02:16:37'),
('bce287de141b24df7fc5a9f1de2d7ceb5a2a2cc5838a0f329ec5d498772718b9f20eeeadd3ba9f7e', 3, 1, 'MyApp', '[]', 0, '2020-10-01 03:13:08', '2020-10-01 03:13:08', '2021-09-30 22:13:08'),
('bd5c46bde1fd605df69da5f7dc9c1646614754fb825e4191f5d0492f9c3693590eee84a1116d3b00', 3, 1, 'MyApp', '[]', 0, '2020-11-18 00:04:12', '2020-11-18 00:04:12', '2021-11-17 19:04:12'),
('c216401ddb0a35a40ec8ea0b9b3851dfca56c556ff482d3633e27e3a48c16ec8e68c354a3cbbe797', 4, 1, 'MyApp', '[]', 0, '2020-10-02 22:37:48', '2020-10-02 22:37:48', '2021-10-02 17:37:48'),
('cc3f19adc729093fd62527fcffdb5106133994f35ab567c3362a3aba278f3b63e0dfe953fcc32360', 9, 1, 'MyApp', '[]', 0, '2020-10-02 22:40:41', '2020-10-02 22:40:41', '2021-10-02 17:40:41'),
('cc65ad880624bb873e48d76909aa7153ec4a46a3e9a50fe01d04babbc64fd12743072a9e278fff15', 5, 1, 'MyApp', '[]', 0, '2020-10-02 22:38:13', '2020-10-02 22:38:13', '2021-10-02 17:38:13'),
('d08be89dfe7a2fffeb82c4f1a3ab48841684cfce18d6ff3526ef2a2d3746f8e78c0ea7a82a5e9c12', 13, 1, 'MyApp', '[]', 0, '2020-10-02 22:42:43', '2020-10-02 22:42:43', '2021-10-02 17:42:43'),
('d30a7cbb595ab27b66c9907f03d4ff27fcd72cf8bc90332120b543c12f2a1a0ef460d712be0edb13', 5, 1, 'MyApp', '[]', 0, '2020-10-02 22:38:14', '2020-10-02 22:38:14', '2021-10-02 17:38:14'),
('d75742173eae236421148b0fd0d2113d2b3fb0f2c7665a4f075b643372c081e3e85ca4eda47e0d74', 10, 1, 'MyApp', '[]', 0, '2020-10-02 22:40:46', '2020-10-02 22:40:46', '2021-10-02 17:40:46'),
('d82e6a17579e415e4efdb33e432662386413644560ab370aa31140986dd3f2e18f0575577003620b', 13, 1, 'MyApp', '[]', 0, '2020-10-02 22:42:43', '2020-10-02 22:42:43', '2021-10-02 17:42:43'),
('dd0e030d8a06d063a536228147d60824fc9f1a5c4fbbbce92a0f8a0a525c065335ab0b59533be503', 16, 1, 'MyApp', '[]', 0, '2020-10-02 22:45:12', '2020-10-02 22:45:12', '2021-10-02 17:45:12'),
('e0bbe1d41c2f557624739f04d49af38d6621190d56beab45bdd021b2398120a9dc143be1d3a8d27f', 15, 1, 'MyApp', '[]', 0, '2020-10-02 22:43:03', '2020-10-02 22:43:03', '2021-10-02 17:43:03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'q472nFVWLHydOk57ODvPQBBKxh9HzBURdMPuIPv4', 'http://localhost', 1, 0, 0, '2020-10-01 03:12:52', '2020-10-01 03:12:52'),
(2, NULL, 'Laravel Password Grant Client', 'PaDwTXhAcxXxtBJi7zV1rwhEBsXxgYvq4FuJb5Bc', 'http://localhost', 0, 1, 0, '2020-10-01 03:12:52', '2020-10-01 03:12:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-10-01 03:12:52', '2020-10-01 03:12:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `templates`
--

CREATE TABLE `templates` (
  `id` int(11) NOT NULL,
  `campo1` varchar(200) DEFAULT NULL,
  `campo2` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `templates`
--

INSERT INTO `templates` (`id`, `campo1`, `campo2`, `created_at`, `updated_at`) VALUES
(1, 'company', 'state', '2020-12-09 03:48:40', '2020-12-09 03:48:40');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'sergio miguel', 'sergiqz@gmail.com', NULL, '$2y$10$tnkPNAAx6Ems99oCZegM6.yPIYtrSmeaaFuSZpdxXVib0Kw.VuQRe', NULL, '2020-10-01 01:48:49', '2020-10-01 01:48:49'),
(3, 'sergio manuel', 'manuel@gmail.com', NULL, '$2y$10$RdyNTiaF19bn.FgjsZJ7H.qO7AjeRPjM7HTKq4UGUaoQ7vRxU3ALi', NULL, '2020-10-01 01:52:49', '2020-10-01 01:52:49'),
(22, 'walky sadfasd', 'walky@gmail.com', NULL, '$2y$10$jjSh9vqz4cUbIW35zlf/Te0Z1UUG3scgm4fWMe3ZnYBhxGaKIVJSO', NULL, '2020-10-23 07:14:22', '2020-10-23 07:14:22'),
(23, 'juan asd', 'juan@gmail.com', NULL, '$2y$10$PWO7YNos43BoGGwlVyQSSeKGU3nmWnsB1DK7MvNyrcUiEmVCaA1Z2', NULL, '2020-11-27 11:17:21', '2020-11-27 11:17:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `webs`
--

CREATE TABLE `webs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `URL` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `webs`
--

INSERT INTO `webs` (`id`, `name`, `URL`, `state`, `created_at`, `updated_at`) VALUES
(9, 'pe.jobrapido', 'https://pe.jobrapido.com', 'Activo', '2020-12-08 20:57:12', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `datascrapings`
--
ALTER TABLE `datascrapings`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `historial_webscrapings`
--
ALTER TABLE `historial_webscrapings`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indices de la tabla `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indices de la tabla `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indices de la tabla `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `templates`
--
ALTER TABLE `templates`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `webs`
--
ALTER TABLE `webs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `datascrapings`
--
ALTER TABLE `datascrapings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=925;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `historial_webscrapings`
--
ALTER TABLE `historial_webscrapings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `templates`
--
ALTER TABLE `templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `webs`
--
ALTER TABLE `webs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
