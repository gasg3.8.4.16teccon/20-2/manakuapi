<?php

namespace App\Http\Controllers;


use Goutte\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Datascraping;
use App\Historial_webscraping;

use Validator;
use App\Http\Resources\Datascraping as WebResource;
use App\Http\Resources\Datascraping as Historial_webscrapingResource;




class ScraperController extends BaseController
{
    private $datawebscraping = [];
    private $empleoarray = [];
    private $empresaarray = [];
    private $statearray = [];
    private $urlarray = [];
    private $categoriaarray = [];
    

    
    public function get_data() {
        $client = new Client();



        for ($i = 1; $i < 3; $i++) {
            if($i != 1){
                $url = 'https://pe.jobrapido.com/Ofertas-de-trabajo-en-Arequipa?r=auto&p='.$i.'&shm=all&ae=60';
            }else{
                $url = 'https://pe.jobrapido.com/Ofertas-de-trabajo-en-Arequipa?r=auto&shm=all&ae=60';
            }


            $crawler = $client->request('GET', $url);



            $crawler->filter('div.result-item')->each(function ($node) {


                $link = $node->filter('a.result-item__link')->attr("href");



                $arequipaarray = array(" Arequipa "," Caylloma "," Islay "," Camana "," Condesuyos "," La Union "," Castilla "," Caraveli ");

                $administracion = array("Jefe","Gerente","Coordinadora","Coordinador","Gerenta","Jefa", "jefe","jefa");

                $programacion = array("Desarollador","Programador","Java","C++" ,"PHP", "php","desarollador","mysql");
        
                $marina = array("Chief Engineer", "marinero", "Marinero" , "marinero", "marinero", "marinero","marinero","pesquero");


                array_push($this->datawebscraping, $node->text());
                
                $raiz = strstr($node->text(), '¿Quieres',true);

                for($j = 0; $j <= 7; $j++){
                    if (strpos($raiz, $arequipaarray[$j]) !== false) {
                        $var = explode($arequipaarray[$j], $raiz);

                        for($k = 0; $k <= 7; $k++){
                            if(strpos($var[0], $administracion[$k]) !== false){
                                array_push($this->statearray, $arequipaarray[$j]);

                                array_push($this->empleoarray, $var[0]);
        
                                array_push($this->empresaarray, $var[1]);
                                
                                array_push($this->urlarray, $link);
                                array_push($this->categoriaarray, "Administracion");

                                break;
                            }else if(strpos($var[0], $programacion[$k]) !== false){
                                array_push($this->statearray, $arequipaarray[$j]);

                                array_push($this->empleoarray, $var[0]);
        
                                array_push($this->empresaarray, $var[1]);
                                
                                array_push($this->urlarray, $link);
                                array_push($this->categoriaarray, "programacion");

                                break;
                            }else if(strpos($var[0], $marina[$k]) !== false){
                                array_push($this->statearray, $arequipaarray[$j]);

                                array_push($this->empleoarray, $var[0]);
        
                                array_push($this->empresaarray, $var[1]);
                                
                                array_push($this->urlarray, $link);
                                array_push($this->categoriaarray, "marina");

                                break;
                            }
                        }
                    } 


                       
                        
                   
                }
  

              

               });
        }


        $result = $this->store_void();

        return response($result, 200);
        
    }

    private function returnResult() {
        $output = [];
        for ($i = 0; $i <= 9; $i++) {
            $output['description'] = $this->datawebscraping[0];
        }

        return $output;
    }

    public function index()
    {
        $datascrapings = Datascraping::all();

        return $this->sendResponse(WebResource::collection($datascrapings), 'Data Webs retrieved successfully.');
    }

    public function store_void()
    {
        $output = [];
        $output2 = [];
        for ($i = 0; $i <= 8; $i++) {
            $output['description'] = $this->empleoarray[$i];
            $output['state'] = $this->statearray[$i];
            $output['company'] = $this->empresaarray[$i];
            $output['url'] = $this->urlarray[$i];
            $output['categoria'] = $this->categoriaarray[$i];

            
            $validator = Validator::make($output, [
                'description' => 'required',
                'state' => 'required',
                'company' => 'required',
                'url' => 'required',
                'categoria' => 'required'
            ]);

            if($validator->fails()){
                return $this->sendError('Validation Error.', $validator->errors());       
            }


            $datascraping = Datascraping::create($output);

        }
        $now = new \DateTime();
        $output2['fecha'] = $now->format('Y-m-d H:i:s');
        $historial_webscraping = Historial_webscraping::create($output2);
        
        return $this->sendResponse(new WebResource($datascraping), 'Web created successfully.');
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $datascraping = Datascraping::find($id);

        if (is_null($datascraping)) {
            return $this->sendError('Web not found.');
        }

        return $this->sendResponse(new WebResource($datascraping), 'Web retrieved successfully.');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Datascraping $datascraping)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'description' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $datascraping->description = $input['description'];

        $datascraping->save();

        return $this->sendResponse(new WebResource($datascraping), 'Web updated successfully.');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Datascraping $datascraping)
    {
        $datascraping->delete();

        return $this->sendResponse([], 'Web deleted successfully.');
    }
}
