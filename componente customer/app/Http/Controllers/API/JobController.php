<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Validator;
use App\Datascraping;
use DB;
use App\Http\Resources\Datascraping as WebResource;

class JobController extends BaseController
{


    public function jobs(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'description' => 'required',
            'state' => 'required',
            'company' => 'required'
        ]);

        $datascrapings = Datascraping::where('state'," ".$request->state." ")
        ->get();
    
            
        return $this->sendResponse(WebResource::collection($datascrapings), 'Data Webs retrieved successfully.');

    }

}
