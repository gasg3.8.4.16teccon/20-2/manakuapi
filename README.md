# Proyecto Final Tecnologías de Construcción de Software
# Manakuapi
API REST en Laravel 6 webscraping, para la extracción de información en página de empleos

## Código:
 El proyecto tiene 4 componentes, lo cual tendrá que repetir esta instalación para cada uno.

## Instalación (Opción 1):

 En la raíz del proyecto escribir en consola:

 * Escribir composer install
 * cambiar el nombre del archivo example.env a .env
 * importar la base de datos
 * importar la base de datos
 * Escribir php artisan key:generate
 * Escribir php artisan serve

 Abrir http://localhost:8000

## Instalación (Opción 2):

* Hacer 4 copias del archivo docker.
* Luego copiar cada componente en la carpeta src en un docker diferente.
* En la raíz del proyecto de docker escribir en consola:
* Abrir la carpeta docker-compose.yml y cambiar de nombre a la imágenes y los puertos.
* Escribir en consola docker-compose up --d --build
* Importar la base de datos en el MYSQL de docker
* Abrir el proyecto en el puerto asignado

## Funcionalidad

### Login y Registro
* (POST localhost:8000/api/register) y (POST localhost:8000/api/login) customer y data manager tiene que estar operativos
### Templates
* Son los filtros para la busqueda en una pagina (GET PUT POST DELETE localhost:8000/api/templates) customer y data manager tiene que estar operativos
### Webs
* las paginas registradas para hacer web scraping(GET PUT POST DELETE localhost:8000/api/webs) customer, data manager y web filter tiene que estar operativos
### Web Scraping
* get_data la funcion para extraer datos de una web(GET localhost:8000/api/get_data) customer, data manager y web filter tiene que estar operativos
* scraping la funcion para editar borrar o ver un empleo (GET PUT DELETE localhost:8000/api/scraping) customer y data managertiene que estar operativos
### Employment
* employment la funcion para crear editar borrar o ver un empleo (GET PUT DELETE localhost:8000/api/employment) customer y data managertiene que estar operativos
### Reportes
* report_state_web el estado de las webs donde se extrae datos (GET localhost:8000/api/report_state_web) customer , data manager y report manager tiene que estar operativos
* ultima_actua_web_sraping el historia de la extraccion de datos (GET localhost:8000/api/ultima_actua_web_sraping) customer , data manager y report manager tiene que estar operativos
* porcentaje_categoria_de_trabajo el porcentaje de trabajos segun su categoria (GET localhost:8000/api/porcentaje_categoria_de_trabajo) customer , data manager y report manager tiene que estar operativos

## Creador:

Sergio Miguel Dueñas Vera

sduenasv@ulasalle.edu.pe