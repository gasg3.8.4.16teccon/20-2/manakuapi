<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Datascraping;
use Validator;
use App\Http\Resources\Datascraping as WebResource;

class DatawebController extends BaseController
{


    public function index()
    {
        $webs = Datascraping::all();

        return $this->sendResponse(WebResource::collection($webs), 'Webs retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();


        $validator = Validator::make($input, [
            'description' => 'required',
            'state' => 'required',
            'company' => 'required'

        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $web = Datascraping::create($input);

        return $this->sendResponse(new WebResource($web), 'Datascraping created successfully.');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $web = Datascraping::find($id);

        if (is_null($web)) {
            return $this->sendError('Datascraping not found.');
        }

        return $this->sendResponse(new WebResource($web), 'Datascraping retrieved successfully.');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Datascraping $web)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'description' => 'required',
            'state' => 'required',
            'company' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $web->description = $input['description'];
        $web->state = $input['state'];
        $web->company = $input['company'];

        $web->save();

        return $this->sendResponse(new WebResource($web), 'Datascraping updated successfully.');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Datascraping $web)
    {
        $web->delete();

        return $this->sendResponse([], 'Datascraping deleted successfully.');
    }
}
