<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Web;
use Validator;
use App\Http\Resources\Web as WebResource;

class WebController extends BaseController
{

 /**
     * @SWG\Get(
     *   path="/api/webs",
     *   security={
     *     {"passport": {}},
     *   },
     *   summary="Get Webs",
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error"),
	 *		@SWG\Parameter(
     *          name="mytest",
     *          in="path",
     *          required=true, 
     *          type="string" 
     *      ),
     * )
     *
     *      * @SWG\Post(
     *   path="/api/webs",
     *   security={
     *     {"passport": {}},
     *   },
     *   summary="Get Webs",
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error"),
	 *		@SWG\Parameter(
     *          name="mytest",
     *          in="path",
     *          required=true, 
     *          type="string" 
     *      ),
     * )
     *
     * 
     *      * @SWG\Delete(
     *   path="/api/webs/{mypage}",
     *   security={
     *     {"passport": {}},
     *   },
     *   summary="Get Webs",
     *   operationId="mypage",
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error"),
	 *		@SWG\Parameter(
     *          name="mypage",
     *          in="path",
     *          required=true, 
     *          type="string" 
     *      ),
     * )
     *
     *    * @SWG\Put(
     *   path="/api/webs",
     *   security={
     *     {"passport": {}},
     *   },
     *   summary="Get Webs",
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error"),
	 *		@SWG\Parameter(
     *          name="mytest",
     *          in="path",
     *          required=true, 
     *          type="string" 
     *      ),
     * )
     *
     */
        
    public function index()
    {
        $webs = Web::all();

        return $this->sendResponse(WebResource::collection($webs), 'Webs retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();


        $validator = Validator::make($input, [
            'name' => 'required',
            'URL' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $web = Web::create($input);

        return $this->sendResponse(new WebResource($web), 'Web created successfully.');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $web = Web::find($id);

        if (is_null($web)) {
            return $this->sendError('Web not found.');
        }

        return $this->sendResponse(new WebResource($web), 'Web retrieved successfully.');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Web $web)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required',
            'URL' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $web->name = $input['name'];
        $web->URL = $input['URL'];
        $web->save();

        return $this->sendResponse(new WebResource($web), 'Web updated successfully.');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Web $web)
    {
        $web->delete();

        return $this->sendResponse([], 'Web deleted successfully.');
    }
}
