<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Web;
use App\Historial_webscraping;
use App\Datascraping;
use Validator;
use App\Http\Resources\Web as WebResource;
use App\Http\Resources\Historial_webscraping as Historial_webscrapingResource;
use App\Http\Resources\Datascraping as DatascrapingResource;
use DB;

class ReportController extends BaseController
{
    public function estado_web()
    {
        $webs = Web::all();

        return $this->sendResponse(WebResource::collection($webs), 'Reporte de Paginas Webs');
    }

    public function ultima_actua_web_sraping()
    {
        $datascraping = Historial_webscraping::all();

        return $this->sendResponse(Historial_webscrapingResource::collection($datascraping), 'Historial de Web scraping');
    }

    public function porcentaje_categoria_de_trabajo()
    {

        $output = [];
        //Select Grade, (Count(Grade)* 100 / (Select Count(*) From MyTable)) as Score From MyTable Group By Grade



        //$datascraping = Datascraping::all();
        
        $sub = Datascraping::all()->count();
        $querya = Datascraping::where('categoria','Administracion')->count();
        $queryp = Datascraping::where('categoria','programacion')->count();
        $querym = Datascraping::where('categoria','marina')->count();
        $queryaa= ($querya/$sub)*100;
        $querypp= ($queryp/$sub)*100;
        $querymm= ($querym/$sub)*100;


        $output['Administracion'] = $queryaa.'%';
        $output['Marina'] = $querymm.'%';
        $output['Programacion'] = $querypp.'%';

    

        return $this->sendResponse($output, 'Porcentaje de empleos');
    }
}
