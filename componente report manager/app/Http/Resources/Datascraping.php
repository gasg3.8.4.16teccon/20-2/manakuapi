<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Datascraping extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'description' => $this->description,
            'company' => $this->company,
            'url' => $this->url,
            'categoria' => $this->categoria,
            'created_at' => $this->created_at->format('d/m/Y')

            

        ];
    }
}
