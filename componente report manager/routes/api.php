<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register', 'API\RegisterController@register');
Route::post('login', 'API\RegisterController@login');
  
Route::middleware('auth:api')->group( function () {
    Route::resource('products', 'API\ProductController');
    Route::resource('webs', 'API\WebController');
    Route::get('get_data', 'ScraperController@get_data');
    Route::resource('scraping', 'ScraperController');
    Route::resource('employment', 'API\DatawebController');
    Route::get('report_state_web', 'API\ReportController@estado_web');
    Route::get('ultima_actua_web_sraping', 'API\ReportController@ultima_actua_web_sraping');
    Route::get('porcentaje_categoria_de_trabajo', 'API\ReportController@porcentaje_categoria_de_trabajo');
});